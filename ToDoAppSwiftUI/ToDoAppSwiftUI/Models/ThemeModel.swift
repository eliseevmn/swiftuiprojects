import SwiftUI

// MARK: - Theme model
struct Theme: Identifiable {
    let id: Int
    let themeName: String
    let themeColor: Color
}
