import SwiftUI

struct MapContainerView: View {

    @ObservedObject var covidFech = CovidFetchRequest()

    var body: some View {
        MapView(countryData: $covidFech.allCountries)
    }
}

struct MapContainerView_Previews: PreviewProvider {
    static var previews: some View {
        MapContainerView()
    }
}
