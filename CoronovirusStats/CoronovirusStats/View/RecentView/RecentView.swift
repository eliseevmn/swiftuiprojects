import SwiftUI

struct RecentView: View {

    @ObservedObject var covidFetch = CovidFetchRequest()
    @ObservedObject var myFetch = MyFetchRequest()
    @ObservedObject var test = CountryStatisticsFetchRequest()

    @State var searchText = ""
    @State var isSearchVisible = false

    var body: some View {

        NavigationView {
            VStack {

                if isSearchVisible {
                    SearchView(searchText: $searchText)
                }

                TotalDataView(totalData: covidFetch.totalData)
                ListHeaderView()

                List {
                    ForEach(myFetch.allCountriesCodable.filter {
                        self.searchText.isEmpty ? true : $0.country.lowercased().contains(self.searchText.lowercased())
                    }, id: \.country) { countryData in
                        NavigationLink(destination: CountryDetailView(countryName: countryData.country)) {
                            CountryDataRowView(countryData: countryData)
                        }
                    }
                } // : Stack
            } // : VStack
                .navigationBarTitle("Recent Data", displayMode: .inline)
                .navigationBarItems(trailing:
                    Button(action: {
                        self.isSearchVisible.toggle()
                        if !self.isSearchVisible {
                            self.searchText = ""
                        }
                    }, label: {
                        Image(systemName: "magnifyingglass")
                    })
            )
        } // : NavigationView
    }
}

struct RecentView_Previews: PreviewProvider {
    static var previews: some View {
        RecentView()
    }
}
