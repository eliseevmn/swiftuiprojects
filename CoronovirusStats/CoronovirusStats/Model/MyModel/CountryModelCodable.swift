import Foundation

struct CountryModelCodable: Codable {
    let country: String
    let data: DataCountry
}

// MARK: - DataClass
struct DataCountry: Codable {
    let newdeaths, newcases, deaths, serious: Int64?
    let recovered: Int64?
    let cases: Int64?
}
