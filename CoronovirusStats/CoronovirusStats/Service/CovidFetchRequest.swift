import SwiftUI
import Alamofire
import SwiftyJSON

class CovidFetchRequest: ObservableObject {

    @Published var allCountries: [CountryData] = []
    @Published var totalData: TotalData = testTotalData

    let headers: HTTPHeaders = [
        "x-rapidapi-host": "covid-19-data.p.rapidapi.com",
        "x-rapidapi-key": "fc5470081bmsh0f6e3bf8a2eff7bp15c5ebjsnd659d1eaa07c"
    ]

    init() {
        getCurrentTotal()
        getAllCountries()
    }

    func getCurrentTotal() {
        AF.request("https://covid-19-data.p.rapidapi.com/totals?format=json", headers: headers).responseJSON { response in

            guard let result = response.data else {
                self.totalData = testTotalData
                return
            }
            let json = JSON(result)

            let confirmed = json[0]["confirmed"].intValue
            let deaths = json[0]["deaths"].intValue
            let critical = json[0]["critical"].intValue
            let recovered = json[0]["recovered"].intValue

            self.totalData = TotalData(confirmed: confirmed,
                                       critical: critical,
                                       deaths: deaths,
                                       recovered: recovered)
        }
    }

    func getAllCountries() {
        AF.request("https://covid-19-data.p.rapidapi.com/country/all?format=json", headers: headers).responseJSON { response in

            var allCount: [CountryData] = []
            guard let result = response.value else {
                return
            }
            print(result)
            guard let dataDictionary = result as? [Dictionary<String, AnyObject>] else { return }
            for countryData in dataDictionary {
                let country = countryData["country"] as? String ?? "Error"
                let longitude = countryData["longitude"] as? Double ?? 0.0
                let latitude = countryData["latitude"] as? Double ?? 0.0
                let confirmed = countryData["confirmed"] as? Int64 ?? 0
                let deaths = countryData["deaths"] as? Int64 ?? 0
                let critical = countryData["critical"] as? Int64 ?? 0
                let recovered = countryData["recovered"] as? Int64 ?? 0

                let countryObject = CountryData(country: country, confirmed: confirmed, critical: critical, deaths: deaths, recovered: recovered, longitude: longitude, latitude: latitude)
                allCount.append(countryObject)
            }

            self.allCountries = allCount.sorted(by: { $0.confirmed > $1.confirmed })
        }
    }
}
