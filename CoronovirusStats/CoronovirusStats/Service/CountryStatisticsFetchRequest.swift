import SwiftUI
import Alamofire
import SwiftyJSON

class CountryStatisticsFetchRequest: ObservableObject {

    @Published var detailedCountryData: DetailedCountryData?

    let headers: HTTPHeaders = [
        "x-rapidapi-host": "covid-193.p.rapidapi.com",
        "x-rapidapi-key": "fc5470081bmsh0f6e3bf8a2eff7bp15c5ebjsnd659d1eaa07c"
    ]

    init() { }

    func getStatsFor(country: String) {
        AF.request("https://covid-193.p.rapidapi.com/statistics?country=\(country)", headers: headers).responseJSON { response in

            guard let result = response.data else {
                return
            }
            let json = JSON(result)

            let country = json["response"][0]["country"].stringValue
            let deaths = json["response"][0]["deaths"]["total"].intValue
            let newDeaths = json["response"][0]["deaths"]["new"].intValue
            let tests = json["response"][0]["tests"]["total"].intValue

            let criticalCases = json["response"][0]["cases"]["critical"].intValue
            let totalCases = json["response"][0]["cases"]["total"].intValue
            let activeCases = json["response"][0]["cases"]["active"].intValue
            let newCases = json["response"][0]["cases"]["new"].intValue
            let recoveredCases = json["response"][0]["cases"]["recovered"].intValue

            self.detailedCountryData = DetailedCountryData(country: country,
                                                      confirmedCases: totalCases,
                                                      newCases: newCases,
                                                      recoveredCases: recoveredCases,
                                                      criticalCases: criticalCases,
                                                      activeCases: activeCases,
                                                      deaths: deaths,
                                                      newDeaths: newDeaths,
                                                      testDone: tests)
        }
    }
}
