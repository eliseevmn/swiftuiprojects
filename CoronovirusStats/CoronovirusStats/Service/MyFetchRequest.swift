import SwiftUI
import Alamofire
import SwiftyJSON

class MyFetchRequest: ObservableObject {

    @Published var allCountriesCodable: [CountryData] = []

    let headers: HTTPHeaders = [
        "x-rapidapi-host": "covid1935.p.rapidapi.com",
        "x-rapidapi-key": "fc5470081bmsh0f6e3bf8a2eff7bp15c5ebjsnd659d1eaa07c"
    ]

    init() {
        getAllCountries()
    }

    func getAllCountries() {
        AF.request("https://covid1935.p.rapidapi.com/api/v1/current", headers: headers).responseData { (data) in
            guard let data = data.data else { return }

            let decoder = JSONDecoder()

            var allCount: [CountryData] = []
            do {
                let objects = try decoder.decode([CountryModelCodable].self, from: data)
                objects.forEach { (model) in
                    let countryData = CountryData(country: model.country,
                                                  confirmed: model.data.cases ?? 0,
                                                  critical: model.data.serious ?? 0,
                                                  deaths: model.data.deaths ?? 0,
                                                  recovered: model.data.recovered ?? 0,
                                                  longitude: 0.0,
                                                  latitude: 0.0)
                    allCount.append(countryData)
                }

                self.allCountriesCodable = allCount.sorted(by: { $0.country < $1.country })
            } catch {
                print(error.localizedDescription)
            }
        }
    }
}
